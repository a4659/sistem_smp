<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    public function getSubMenu()
    {
        $query = "SELECT user_sub_menu.*, user_menu.menu FROM user_sub_menu JOIN user_menu ON user_sub_menu.menu_id = user_menu.id ";

        return $this->db->query($query)->result_array();
    }
    function edit($id)
    {
        $where = array('id' => $id);
        $query = "SELECT * FROM user_role WHERE=$id ORDER BY id ASC ";
        //$data['user'] = $this->m_data->edit_data($where,'user')->result();
        //$this->load->view('v_edit',$data);
    }

    public function deleteSubMenu($id)
    {
        $query = $this->db->table('user_menu_company')->delete(array('id' => $id));
        return $query;
    }

    public function getsponsor()
    {
        $query = "SELECT * FROM sponsor ORDER BY id ASC ";

        return $this->db->query($query)->result_array();
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('m_submenu');
        $this->load->model('m_menucompany');
        $this->load->helper('url');
    }

    public function index()
    {
        $data['title'] = 'Menu Management';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['menur'] = $this->db->get('user_sub_menu')->result_array();

        //echo 'Selamat Datang' . $data['user']['name'];

        $this->form_validation->set_rules('menu', 'Menu', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/index', $data);
            $this->load->view('templates/footer');
            //$this->load->view('user/index', $data);
        } else {
            $this->db->insert('user_menu', ['menu' => $this->input->post('menu')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New menu added!</div>');
            redirect('menu');
        }
    }

    public function subedit()
    {
        $data['title'] = 'Edit Submenu';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['userwe'] = $this->db->get('user')->result_array();

        // $this->form_validation->run() == false) {
        $this->form_validation->set_rules('name', 'Full Name', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('user/edit', $data);
            $this->load->view('templates/footer');
        } else {
            $name = $this->input->post('name');
            $email = $this->input->post('email');

            //cek gambar yang di pilih
            $upload_image = $_FILES['image']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['upload_path'] = './assets/img/profile/';

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $old_image = $data['user']['image'];
                    if ($old_image != 'default.jpg') {
                        unlink(FCPATH . 'assets/img/profile/' . $old_image);
                    }

                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                } else {
                    echo $this->upload->dispay_errors();
                }
            }
            // var_dump($upload_image);
            // die;

            $this->db->set('name', $name);
            $this->db->where('email', $email);
            $this->db->update('user');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Akun Anda Sukses Dirubah!</div>');
            redirect('user');
        }
    }

    public function submenu()
    {
        $data['title'] = 'Submenu Management';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['submenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'icon', 'required');


        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active')
            ];
            $this->db->insert('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Submenu added!</div>');
            redirect('menu/submenu');
        }
    }

    public function submenueditm($id)
    {
        $data['title'] = 'Edit Data Menu';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $where = array('id' => $id);
        //$this->load->model('Menu_model', 'menu');
        //$data['edit'] = $this->menu->getSubMenu();
        $data['menue'] = $this->db->get('user_menu')->result_array();
        $data['menu'] = $this->m_submenu->edit_data($where, 'user_sub_menu')->result();
        //$this->load->view('admin/editrole', $data);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('menu/submenuedit', $data);
        $this->load->view('templates/footer');
    }

    public function updatemenu()
    {
        //$this->form_validation->set_rules('role', 'Role', 'required|trim');
        $id = $this->input->post('id');
        $title = $this->input->post('title');
        $menu = $this->input->post('menu_id');
        $url = $this->input->post('url');
        $icon = $this->input->post('icon');
        $is_active = $this->input->post('is_active');

        $data = array(
            'title' => $title,
            'menu_id' => $menu,
            'url' => $url,
            'icon' => $icon,
            'is_active' => $is_active
        );

        $where = array(
            'id' => $id
        );

        $this->m_menucompany->update_data($where, $data, 'user_sub_menu');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Menu Update!</div>');
        //$this->m_data->update($where, $data, 'user_role');
        redirect('menu/submenu');
        // var_dump($data);
        // die;
    }

    public function addsponsor()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['sponsor'] = $this->db->get('sponsor')->result_array();
        $data['menu'] = $this->db->get('sponsor')->result_array();

        $data = array(
            'name' => $this->input->post('name'),
            'is_active' => $this->input->post('is_active'),
        );

        if (!empty($_FILES['image']['name'])) {
            $image = $this->_do_upload();
            $data['image'] = $image;
        }

        $this->sponsor_model->insert($data);
        redirect('menu/sponsor', $data);
    }

    private function _do_upload()
    {
        $image_name = time() . '-' . $_FILES["image"]['name'];

        $config['upload_path']         = 'assets/img/profile/';
        $config['allowed_types']     = 'gif|jpg|png';
        $config['max_size']         = 100;
        $config['max_widht']         = 1000;
        $config['max_height']          = 1000;
        $config['file_name']         = $image_name;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $this->session->set_flashdata('msg', $this->upload->display_errors('', ''));
            redirect('');
        }
        return $this->upload->data('file_name');
    }

    public function sponsor()
    {
        $data['title'] = 'Menu Sponsor';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['sponsor'] = $this->db->get('sponsor')->result_array();

        //$this->load->model('Menu_model', 'menu');

        //$data['sponsor'] = $this->menu->getsponsor();
        $data['menu'] = $this->db->get('sponsor')->result_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('menu/sponsor', $data);
        $this->load->view('templates/footer');
    }

    //function menu company
    public function submenucom()
    {
        $data['title'] = 'Menu Sadhana Company';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu_company')->result_array();

        //echo 'Selamat Datang' . $data['user']['name'];

        $this->form_validation->set_rules('menu', 'Menu', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('url', 'Url', 'required');
        $this->form_validation->set_rules('icon', 'Icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menucom/index', $data);
            $this->load->view('templates/footer');
            //$this->load->view('user/index', $data);
        } else {
            $data = [
                'menu' => $this->input->post('menu'),
                'title' => $this->input->post('title'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active'),
            ];
            $this->db->insert('user_menu_company', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Menu added!</div>');
            redirect('menucom/submenu');
        }
    }

    public function editmenu($id)
    {
        $data['title'] = 'Edit Menu';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('M_menucompany', 'menu');
        $where = array('id' => $id);
        $data['menu'] = $this->m_menucompany->edit_data($where, 'user_menu_company')->result();
        //$this->load->view('admin/editrole', $data);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('menucom/editmenu', $data);
        $this->load->view('templates/footer');
    }

    function hapusrole($id)
    {
        $where = array('id' => $id);
        $this->m_submenu->hapus_data($where, 'user_sub_menu');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Berhasil di Delete!</div>');
        redirect('menu/submenu');
    }
}

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-lg-8">
                    <br>
                    <!-- <?= form_open_multipart('user/edit'); ?> -->
                    <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                    <?= $this->session->flashdata('message'); ?>

                    <?php foreach ($role as $u) { ?>
                        <form action="<?php echo base_url() . 'admin/updaterole/'; ?>" method="post">
                            <div class="form-group row">
                                <label for="role" class="col-sm-2 col-form-label">Role</label>
                                <div class="col-sm-10">
                                    <input type="hidden" name="id" value="<?php echo $u->id ?>">
                                    <input type="text" class="form-control" id="role" name="role" value="<?php echo $u->role; ?>">
                                </div>
                            </div>


                            <div class="form-group row justify-content-end">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary">Edit</button>
                                </div>
                            </div>
                            <!-- <form action="" method="" enctype="multipart/form-data"> -->

                        </form>
                </div>
            </div>
        <?php } ?>



        </div>
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-lg-8">
                    <br>
                    <!-- <?= form_open_multipart('menucom/editmenu'); ?> -->
                    <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                    <?= $this->session->flashdata('message'); ?>

                    <?php foreach ($menu as $u) { ?>
                        <form action="<?php echo base_url() . 'menucom/updatemenu/'; ?>" method="post">
                            <div class="form-group row">
                                <label for="role" class="col-sm-2 col-form-label">Menu</label>

                                <div class="col-sm-10">
                                    <input type="hidden" name="id" value="<?php echo $u->id ?>">

                                    <input type="text" class="form-control" id="menu" name="menu" value="<?php echo $u->menu; ?>">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="role" class="col-sm-2 col-form-label">Title</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="title" name="title" value="<?php echo $u->title; ?>">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="role" class="col-sm-2 col-form-label">Url</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="url" name="url" value="<?php echo $u->url; ?>">
                                </div>
                            </div>

                            <!-- <div class="form-group row">
                                <label for="role" class="col-sm-2 col-form-label">Icon</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="icon" name="icon" value="<?php echo $u->icon; ?>">
                                </div>
                            </div> -->

                            <!-- <div class="form-group row">
                                <div class="col-sm-10">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="1" name="is_active" id="is_active" checked>
                                        <label class="form-check-label" for="is_active">
                                            Active?
                                        </label>
                                    </div>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label for="is_active" class="col-sm-2 col-form-label">Status</label>
                                <div class="col-sm-10">
                                    <label><input type="radio" name="is_active" value="1" <?php echo ($u->is_active == '1' ? ' checked' : ''); ?>> Active</label>
                                    <label><input type="radio" name="is_active" value="0" <?php echo ($u->is_active == '0' ? ' checked' : ''); ?>> No Active</label>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <select name="is_active" id="is_active" class="form-control">
                                    <option value="<?php echo $u->is_active; ?>">Select Menu</option>
                                    <option value="1">Active</option>
                                    <option value="0">NO Active</option>
                                    </option>
                                </select>
                            </div> -->

                </div>



            </div>
            <br>
            <div class="form-group row">
                <div class="form-group row justify-content-end">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </div>
            </div>
            <!-- <form action="" method="" enctype="multipart/form-data"> -->

            </form>

        </div>
    <?php } ?>



    </div>
</div>
</div>
<!-- End of Main Content -->